var express = require('express');
var grupo = require('../model/grupo');
var router = express.Router();

router.get('/api/Grupo', function(req, res) {
    grupo.getAll(function(error, resultados) {
        if (error) {
            res.json(error);
        } else {
            res.json(resultados);
        }
    })
});

router.post('/api/Grupo', function(req, res) {
    var data = {
        'nombre': req.body.nombre,
        'idSeccion': req.body.idSeccion
    };
    
    grupo.insert(data, function(error, resultados) {
        if (error) {
            res.json(error);
        } else {
            res.json({'insert': resultados.insertId});
        }
    });
});

module.exports = router;