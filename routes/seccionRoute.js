var express = require('express');
var seccion = require('../model/seccion')
var router = express.Router();

router.get('/api/Seccion', function(req, res) {
    seccion.getAll(function(error, resultados) {
        if (error) {
            res.json(error);
        } else {
            res.json(resultados);
        }
    });
});

module.exports = router;