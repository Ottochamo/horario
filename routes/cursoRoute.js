var express = require('express');
var curso = require('../model/curso');

var router = express.Router();

router.get('/api/Curso', function(req, res) {
    curso.getAll(function(error, resultados) {
        if (error) {
            res.json(error);
        } else {
            res.json(resultados);
        }
    });
});

router.post('/api/Detalle', function(req, res) {
    var data = {
        'idGrupo': req.body.idGrupo,
        'idCurso': req.body.idCurso
    };
    curso.insertDetail(data, function(error, resultados) {
        if (error) {
            console.log(error);
            res.json(error);
        } else {
            res.json(resultados);
        }
    });
});

router.get('/api/Detalle/:id', function(req, res) {
    var id = req.params.id;

    curso.getDetalle(id, function(error, resultados) {
        if (error) {
            res.json(error);
        } else {
            res.json(resultados);
        }
    })

});

module.exports = router;