CREATE DATABASE horario;

USE horario;

CREATE TABLE Seccion (
	idSeccion INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(1) NOT NULL,
    PRIMARY KEY (idSeccion)
);

CREATE TABLE Grupo (
	idGrupo INT NOT NULL AUTO_INCREMENT,
    idSeccion INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    PRIMARY KEY(idGrupo),
    UNIQUE (nombre, idSeccion),
    FOREIGN KEY(idSeccion) REFERENCES Seccion(idSeccion)
);

CREATE TABLE Curso (
	idCurso INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    UNIQUE (nombre),
    PRIMARY KEY (idCurso)
);

CREATE TABLE DetalleGrupo (
    idGrupo INT NOT NULL,
    idCurso INT NOT NULL,
    PRIMARY KEY (idGrupo, idCurso),
    FOREIGN KEY (idGrupo) REFERENCES Grupo (idGrupo),
    FOREIGN KEY (idCurso) REFERENCES Curso (idCurso)
);

INSERT INTO DetalleGrupo(idGrupo, idCurso) VALUES(1, 3)

SELECT * FROM detalleGrupo

DROP DATABASE horario