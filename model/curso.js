var database = require('./database/connection');

var curso = {};

curso.getAll = function(callback) {
    if (database) {
        var sql = 'SELECT * FROM curso';

        database.query(sql, function(error, resultados) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultados);
            }
        });

    }
}

curso.insert = function(data, callback) {
    if (database) {
        var sql = 'CALL createCurso(?)';
        database.query(sql, data.nombre, function(error, resultados) {
            if (error) {
                callback(error, null);
            } else {
                var d = {
                    'idGrupo': data.idGrupo,
                    'idCurso': resultados.insertId,
                };
                curso.insertDetail(d, callback);
            }
        });
    }
}

curso.insertDetail = function(data, callback) {
    if (database) {
        var sql = 'CALL createDetalleGrupo(?, ?)';
        var values = [data.idGrupo, data.idCurso];

        database.query(sql, values, function(error, resultados) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultados);
            }
        });

    }
}

curso.getDetalle = function(idGrupo, callback) {
    if (database) {
        var sql = 'SELECT * FROM detalles WHERE idGrupo = ?';
        database.query(sql, idGrupo, function(error, resultados) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultados);
            }
        });
    }
}

module.exports = curso;