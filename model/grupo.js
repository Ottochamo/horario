var database = require('./database/connection');

var grupo = {};

grupo.getAll = function(callback) {
    if (database) {
        var sql = 'SELECT * FROM grupos_ordenados';
        database.query(sql, function(error, resultados) {
            if (error) {
                throw error;
            } else {
                callback(null, resultados);
            }
        })
    }
}

grupo.insert = function(data, callback) {
    if (database) {
        var sql = 'call createGrupo(?, ?)';
        var values = [data.nombre, data.idSeccion];
        database.query(sql, values, function(error, resultados) {
            if (error) {                
                callback(error, null);
            } else {
                callback(null, resultados.insertId);
            }
        });
    }
}

module.exports = grupo;