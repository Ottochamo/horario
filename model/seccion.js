var database = require('./database/connection');

var seccion = {};

seccion.getAll = function(callback) {
    if (database) {
        var sql = 'SELECT * FROM seccion';
        database.query(sql, function(error, resultados) {
            if (error) {
                throw error;
            } else {
                callback(null, resultados);
            }
        });
    }
}

module.exports = seccion;