function ViewModel () {
    var seccionUri = '/api/Seccion/';
    var grupoUri = '/api/Grupo/';
    var cursoUri = '/api/Curso/';
    var detalleUri = '/api/Detalle/';

    var main = this;

    main.grupoNuevo = {
        'nombre': ko.observable(),
        'idSeccion': ko.observable()
    };

    main.grupos = ko.observableArray();
    main.secciones = ko.observableArray();
    main.cursos = ko.observableArray();
    main.grupoSeleccion = ko.observable();
    main.detallesw = ko.observableArray();

    main.curso = {
        'idCurso': ko.observable()
    };

    main.crearGrupo = function() {
        var data = {
            'nombre': main.grupoNuevo.nombre(),
            'idSeccion': main.grupoNuevo.idSeccion()
        };
        
        ajaxHelper(grupoUri, 'POST', data).done(function() {
            main.getGrupos();
        });
    }

    main.cargarDetalles = function(item) {
        var uri = detalleUri + item.idGrupo;
        ajaxHelper(uri, 'GET').done(function(data) {
            main.detallesw('');
            main.detallesw(data);
        });

    }

    main.crearDetalle = function () {
        var data = {
            'idGrupo': main.grupoSeleccion().idGrupo,
            'idCurso': main.curso.idCurso()
        };

        ajaxHelper(detalleUri, 'POST', data).done(function() {
            console.log('todo bien');
        })

    }

    main.getSecciones = function() {
        ajaxHelper(seccionUri, 'GET').done(function(data) {
            main.secciones(data);
        });
    }

    main.getGrupos = function() {
        ajaxHelper(grupoUri, 'GET').done(function(data) {
            main.grupos(data); 
        });
    }

    main.getCursos = function() {
        ajaxHelper(cursoUri, 'GET').done(function(data) {
            main.cursos(data);
        });
    }

    main.getDetalles = function() {
        ajaxHelper(detalleUri, 'GET').done(function(data) {
            main.detalles(data);
        });
    }

    main.getCursos();
    main.getSecciones();
    main.getGrupos();

}

function ajaxHelper(uri, method, data) {
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        });
}

$(document).ready(function() {
    ko.applyBindings(new ViewModel());
})