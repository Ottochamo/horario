USE horario;

DELIMITER &&
CREATE PROCEDURE createGrupo(_nombre VARCHAR(100), _idSeccion INT)
	BEGIN
		INSERT INTO Grupo(nombre, idSeccion) VALUES(_nombre, _idSeccion);
    END&&
DELIMITER ;

DELIMITER &&
CREATE PROCEDURE createSeccion(_nombre VARCHAR(1))
	BEGIN
		INSERT INTO Seccion(nombre) VALUES(_nombre);
	END&&
DELIMITER ;

CALL createSeccion('A');
CALL createSeccion('B');
CALL createSeccion('C');

CALL createGrupo('INFORMATICA', 1)

CALL createCursoAndDetalle('IDIOMA', 1)

DELIMITER &&
CREATE PROCEDURE createCurso(_nombre VARCHAR(100))
	BEGIN
		DECLARE _idCurso INT;
        INSERT INTO Curso(nombre) VALUES(_nombre);       
    END&&
DELIMITER ;



DELIMITER &&
CREATE PROCEDURE createDetalleGrupo(_idCurso INT, _idGrupo INT)
	BEGIN
		INSERT INTO DetalleGrupo(idGrupo, idCurso) VALUES (_idGrupo, _idCurso);
    END&&
DELIMITER ;

SELECT * FROM grupos_ordenados

USE HORARIO

CREATE VIEW grupos_ordenados
	AS
    SELECT s.nombre, g.idGrupo, g.nombre AS grupo FROM Grupo AS G 
    INNER JOIN seccion AS s ON s.idSeccion = g.idSeccion
    ORDER BY g.idSeccion


CREATE VIEW detalles
	AS 
    SELECT 	g.idGrupo, g.nombre AS grupo, c.idCurso, c.nombre FROM Grupo AS g
	INNER JOIN DetalleGrupo AS d ON d.idGrupo = g.idGrupo
    INNER JOIN Curso AS c ON d.idCurso = c.idCurso